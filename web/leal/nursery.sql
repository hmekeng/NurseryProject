-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 13 avr. 2018 à 15:27
-- Version du serveur :  10.1.24-MariaDB
-- Version de PHP :  7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `nursery`
--

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `nomGroupe` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nomGroupe`) VALUES
(1, 'BSG1'),
(2, 'BSG2'),
(7, 'Section des bÃ©bÃ©s'),
(8, 'Moyen section'),
(9, 'Section des Grands');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

CREATE TABLE `inscription` (
  `id` int(11) NOT NULL,
  `nomEnfant` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomParent` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenomParent` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codePostal` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commune` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateNaiss` date NOT NULL,
  `dateEntree` date NOT NULL,
  `presence` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateDemande` date NOT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`id`, `nomEnfant`, `nomParent`, `prenomParent`, `adresse`, `codePostal`, `commune`, `email`, `telephone`, `dateNaiss`, `dateEntree`, `presence`, `dateDemande`, `utilisateur_id`, `groupe_id`) VALUES
(8, 'test22', 'test1', 'test1', 'rue trois vent', '1000', 'namur', 'test@yahoo.com', '044444', '2018-02-12', '2018-07-07', 'Avant-midi', '2018-03-01', NULL, NULL),
(9, 'aaa', 'abc', 'abc', 'adc', '1120', 'woluwe', 'abc@gmail.com', '02222', '2017-01-01', '2018-01-01', 'JournÃ©e complete', '2017-02-01', NULL, NULL),
(10, 'aaaaaaaaaa', 'kh', 'hgkhgk', 'ghkghkgh', '123123', 'liuoiu', 'abc@gmail.com', '456345546', '1902-01-01', '2013-01-01', 'Avant-midi', '2013-01-01', NULL, NULL),
(11, 'bb', 'bbb', 'bbb', 'aaa', '222', 'anderlecht', 'a@gmail.com', '22222', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', NULL, NULL),
(12, 'bb', 'mael', 'mam', 'mmm', 'mmm', 'kiojk', 'ojko@yahoo.fr', 'ok', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', 6, 1),
(13, 'a', 'aaa', 'az', 'av a', '1002', 'dixieme', 'aa@gmail.com', '02222', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', 6, 1),
(14, 'eee', 'rrr', 'rrr', 'av rr', '1120', 'woluwe st pierre', 'rr@gmail.com', '333', '2018-01-01', '2018-07-01', 'JournÃ©e complete', '2017-01-01', 6, 1),
(15, 'bebe1009', 'parent', 'parent', 'avenu tedesco24', '1160', 'a', 'abc@gmail.com', '+3248888', '2018-01-01', '2018-09-01', 'JournÃ©e complete', '2017-08-18', 7, NULL),
(16, 'aa', 'aa', 'aa', 'aa', 'aa', 'aa', 'a@ezr.r', 'a2222', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', 7, NULL),
(17, 'aa', 'aa', 'aa', 'aa', 'aa', 'aa', 'a@ezr.r', 'a2222', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', 7, NULL),
(18, 'ddd', 'ddd', 'dddd', 'ddd', '200', 'ddd', 'd@gmail.com', '0222', '1902-01-01', '2013-01-01', 'JournÃ©e complete', '2013-01-01', 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sujet` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `inscription_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id`, `email`, `sujet`, `contenu`, `inscription_id`) VALUES
(1, 'admin@gmail.com', 'Demande approuvÃ©e', 'Vous pouvez passÃ© visitÃ© la creche\r\nMerci', NULL),
(2, 'aa@gmail.com', 'aaa', 'aaa', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `puricultrice`
--

CREATE TABLE `puricultrice` (
  `id` int(11) NOT NULL,
  `nomPuric` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenomPuric` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codePostal` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commune` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diplome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `groupe_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `puricultrice`
--

INSERT INTO `puricultrice` (`id`, `nomPuric`, `prenomPuric`, `adresse`, `codePostal`, `commune`, `email`, `telephone`, `sexe`, `diplome`, `groupe_id`) VALUES
(1, 'aaa', 'aaa', 'aaaa', 'aaa', 'aaa', 'a@gmail.com', 'aaaaa', 'Homme', 'aaa', 2),
(2, 'bbbaaaaaaaaaaaaaaa', 'bbb', 'bbbb', '1000', 'bbbb', 'bb@gmail.com', 'bbb', 'Femme', 'cess orientation aide familiale', 7),
(3, 'bbbbssssssss', 'cccccccddd', 'cccccccc', '1000', 'ccc', 'bb@gmail.com', 'cccc', 'Homme', 'cess orientation aide familiale', 9),
(4, 'ddd', 'ddd', 'av', '100', 'bx', 'd@gmail.com', '0333', 'Homme', 'Cess orientation petite enfance', 8),
(5, 'hhh', 'hhhh', 'hhh av', '200', 'molembeek', 'h@gmail.com', '03333', 'Femme', 'bachelier ou graduat Infirmerie', 1),
(6, 'ddd', 'dd', 'hhh av', '1120', 'bruxelle', 'y@gmail.com', '0333', 'Femme', 'bachelier ou graduat Infirmerie', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `password`) VALUES
(1, 'Mekeng saker', 'Hortance Nicole', 'hortance@gmail.com', 'admin'),
(2, 'Dupont', 'Marie', 'Dupont@gmail.com', 'david'),
(3, 'aa', 'bb', 'b@yahoo.fr', '$2y$13$pHwZXSZnFi5JIkMILgytXe0zykNUwom2CkmUCzCYjQI41w6LkOgXO'),
(5, 'Smith', 'Hortence', 'z@z.be', '$2y$13$EUJ77yj8xNRHdqCONgpbFu75KlSOBsvABAYPdDzA6oV03SuMOCnVS'),
(6, 'Dupont', 'sara', 'dsara@yahoo.fr', '$2y$13$bI.GnFqE43Y0QyjkoMQBXuzABX/MwFiv4iR44ww3Pg7BsMdikwlHG'),
(7, 'admin', 'adminhortance', 'admin@gmail.com', '$2y$13$8QySf6S6uIZzE2A/5roFwOjXDWO6qimVy4YvC6fFOiwZ3HMG1uGw6');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5E90F6D6FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_5E90F6D67A45358C` (`groupe_id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307F5DAC5993` (`inscription_id`);

--
-- Index pour la table `puricultrice`
--
ALTER TABLE `puricultrice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B4FE3F817A45358C` (`groupe_id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `inscription`
--
ALTER TABLE `inscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `puricultrice`
--
ALTER TABLE `puricultrice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `FK_5E90F6D67A45358C` FOREIGN KEY (`groupe_id`) REFERENCES `groupe` (`id`),
  ADD CONSTRAINT `FK_5E90F6D6FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F5DAC5993` FOREIGN KEY (`inscription_id`) REFERENCES `inscription` (`id`);

--
-- Contraintes pour la table `puricultrice`
--
ALTER TABLE `puricultrice`
  ADD CONSTRAINT `FK_B4FE3F817A45358C` FOREIGN KEY (`groupe_id`) REFERENCES `groupe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
