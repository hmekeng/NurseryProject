<?php

namespace NurseryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NurseryBundle\Entity\Groupe;
use NurseryBundle\Repository\GroupeRepository;


class PuricultriceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('nomPuric', TextType::class)
                ->add('prenomPuric', TextType::class)
                //->add('diplome', TextType::class)
                ->add('diplome', ChoiceType::class, array(
                   'choices' => array('CESS Orientation petite enfance' =>'Cess orientation petite enfance', 'CESS orientation Aide familialle' => 'cess orientation aide familiale', 'Bachelier ou Graduat Infirmerie' => 'bachelier ou graduat Infirmerie' ),
                 ))
                ->add('adresse', TextType::class)
                ->add('codePostal', TextType::class)
                ->add('commune', TextType::class)
                 ->add ('groupe', EntityType::class, [
                        'class' => Groupe::class,
                        'query_builder' => function (GroupeRepository $er){
                            return $er->createQueryBuilder('g')->orderBy ('g.id','ASC');
                        },
                        'choice_label' => function ($x){
                            return strtoupper($x->getNomGroupe());
                        }
                    ])
                ->add('telephone',TextType::class)
                ->add('email', EmailType::class)
               //->add('diplome');
                 ->add('sexe', ChoiceType::class, array(
                 'choices' => array('Homme' =>'Homme', 'Femme' => 'Femme'), 'expanded'=>true
                  )) ;
               
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NurseryBundle\Entity\Puricultrice'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nurserybundle_puricultrice';
    }


}
