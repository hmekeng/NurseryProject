<?php

namespace NurseryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use NurseryBundle\Entity\Groupe;

class InscriptionEditType extends InscriptionType // Ici, on hérite de ArticleType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        
    // On fait appel à la méthode buildForm du parent, qui va ajouter tous les champs à $builder
    parent::buildForm($builder, $options);

    // On supprime celui qu'on ne veut pas dans le formulaire de modification
       $builder->add ('groupe', EntityType::class, [
                        'class' => Groupe::class,
                        'query_builder' => function (GroupeRepository $er){
                            return $er->createQueryBuilder('g')->orderBy ('g.id','ASC');
                        },
                        'choice_label' => function ($x){
                            return strtoupper($x->getNomGroupe());
                        }
                    ])
                ->remove('nomEnfant',TextType::class)
                ->remove('nomParent',TextType::class)
                ->remove('prenomParent',TextType::class)
                ->remove('adresse',TextType::class)
                ->remove('codePostal',TextType::class)
                ->remove('commune',TextType::class)
                ->remove('email',EmailType::class)
                ->remove('telephone',TextType::class)
                ->remove('dateNaiss',BirthdayType::class)
                ->remove('dateEntree',DateType::class)
                ->remove('presence',ChoiceType::class, array(
                 'choices' => array('Journée complete' =>'Journée complete', 'Avant-midi' => 'Avant-midi', 'Aprés-midi' => 'Aprés-midi' ),
                  ))
                
//                ->add('section', ChoiceType::class, array(
//                   'choices' => array('Bébé' =>'bébé', 'Moyen' => 'moyen', 'Grand' => 'grand' ),
//                  ))
                ->remove('dateDemande',DateType::class);
                //->add('utilisateur')->add('groupe');
        
              
    }/**
     * {@inheritdoc}
     */
    // On modifie cette méthode car les deux formulaires doivent avoir un nom différent
  public function getName()
  {
    return 'nurserybundle_inscriptionEditType';
  }


}
