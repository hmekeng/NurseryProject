<?php

namespace NurseryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class InscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomEnfant',TextType::class)
                ->add('nomParent',TextType::class)
                ->add('prenomParent',TextType::class)
                ->add('adresse',TextType::class)
                ->add('codePostal',TextType::class)
                ->add('commune',TextType::class)
                ->add('email',EmailType::class)
                ->add('telephone',TextType::class)
                ->add('dateNaiss',BirthdayType::class)
                ->add('dateEntree',DateType::class)
                ->add('presence',ChoiceType::class, array(
                 'choices' => array('Journée complete' =>'Journée complete', 'Avant-midi' => 'Avant-midi', 'Aprés-midi' => 'Aprés-midi' ),
                  ))
                
//                ->add('section', ChoiceType::class, array(
//                   'choices' => array('Bébé' =>'bébé', 'Moyen' => 'moyen', 'Grand' => 'grand' ),
//                  ))
                ->add('dateDemande',DateType::class);
                //->add('utilisateur')->add('groupe');
        
              
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NurseryBundle\Entity\Inscription'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nurserybundle_inscription';
    }


}
