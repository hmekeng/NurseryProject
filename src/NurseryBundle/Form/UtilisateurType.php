<?php

namespace NurseryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UtilisateurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class)
                ->add('prenom',TextType::class)
                ->add('email', EmailType::class)
               ->add('passwordOriginal', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array ('label' => 'Mot de passe'),
                'second_options' => array ('label' => 'Répétez le mot de passe')
            ));
    }/**
     * {@inheritdoc}
     */
    
     
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NurseryBundle\Entity\Utilisateur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'nurserybundle_utilisateur';
    }


}
