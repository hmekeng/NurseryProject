<?php

namespace NurseryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NurseryBundle\Entity\Groupe;
use NurseryBundle\Form\GroupeType;
 
class GroupeController extends Controller
{
    
    public function insererGroupeAction(Request $req)
    {
        $groupe=new Groupe();
           
        $formulaire= $this->createForm(GroupeType::class, $groupe);
        $formulaire->handleRequest($req);
        
        if ($formulaire->isSubmitted() && $formulaire->isValid()){
       

            $em = $this->getDoctrine()->getManager();
            $em->persist($groupe);
            $em->flush();
             //redirect vers la page liste des groupes a cree
             return $this->redirectToRoute ("afficherGroupes");

       
        }
        else{
        
            return $this->render("NurseryBundleViews/GroupeControllerViews/inserer_groupe.html.twig",
            array ('formulaire'=>$formulaire->createView()));
               
        }
    }
    
    public function afficherGroupesAction ()
    {
        $em=$this->getDoctrine()->getManager();
        $groupes=$em->getRepository('NurseryBundle:Groupe')->findAll();  
         return $this->render("NurseryBundleViews/GroupeControllerViews/afficher_groupes.html.twig", array('ListeGroupe'=>$groupes));
    }
    
 
    
    
}

