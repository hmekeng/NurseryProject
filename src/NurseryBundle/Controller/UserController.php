<?php
namespace NurseryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NurseryBundle\Entity\Utilisateur;
use NurseryBundle\Form\UtilisateurType;
use NurseryBundle\Entity\Message;
use NurseryBundle\Form\MessageType;


class UserController extends Controller
{

    public function createAccountAction(Request $req)
    {
        $passwordEncoder = $this->get('security.password_encoder');
        
        $utilisateur = new Utilisateur();
    
         $formulaire = $this->createForm(UtilisateurType::class, $utilisateur);
         $formulaire->handleRequest($req);
         
         if ($formulaire->isSubmitted() && $formulaire->isValid()){
       
        $password = $passwordEncoder->encodePassword($utilisateur, $utilisateur->getPasswordOriginal());
       
        $utilisateur->setPassword($password);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($utilisateur);
        $em->flush();
         //redirect vers la page confirmationpage
        return $this->redirectToRoute('connexionPage');

       
        }
        else
        {
                return $this->render 
               ('NurseryBundleViews/UserControllerViews/createAccount.html.twig', 
                    array ('formulaire'=>$formulaire->createView()));
        }
      }
      
      
    public function modifierUserAction (Request $req)
    {
       
       
       
        $em =$this->getDoctrine()->getManager(); 
        
        $user= $em->getRepository('NurseryBundle:Utilisateur')->findOneById($req->get('id'));
        
        
           
        if (is_null($user)){
    //            dump($inscription);
    //           die();
        
            return $this->redirectToRoute('connexionPage');
        }
        
        $form=$this->createForm("NurseryBundle\Form\UtilisateurType", $user, 
                                                array('method'=>'POST',
                                                      'action'=>'/modifierUser'
                                                    )
                                );
        $form->handleRequest($req);
        
        
        // si on a clique Submit
        if ( $form->isSubmitted() &&  $form->isValid()){
           $userModifie = $form->getData();         
           $em->persist($userModifie);     
           $em->flush();
           //$message="l'entité a été modifié avec success!!";
           return $this->redirectToRoute ("afficherUtilisateurs");
                
        }
        // afficher le formulaire
        return $this->render("NurseryBundleViews/UserControllerViews/modifier_user.html.twig", array(
            'form'=>$form->createView(),
            //'msg'=>$message,
            'idUser'=>$req->get('id')
        ));
    }
    
     public function supprimerUserAction(Request $req)        
    {
        //$message="supprimmer user";


        $em=$this->getDoctrine()->getManager();
        $user= $em->find('NurseryBundle:Utilisateur', $req->get('id'));
        if(!$user)
        {
            throw $this->createNotFoundException('l\'utilisateur avec l\'id' .$req->get('id').  'n\'existe pas');
        }
        $em->remove($user);
        $em->flush();

        //$message="le user a été bien supprimée!!!";

        //return new Response ("la demande a été bien supprimée!!!");

         return $this->redirectToRoute ("afficherUtilisateurs");


    }
      
    public function verifieUserAction(){
         
        if($this->getUser()->getEmail()=="admin@gmail.com")
        {
           
         return $this->redirectToRoute ("GestionInscription");
        }
        else
        {
            
         return $this->redirectToRoute ("formViewInscription");
        }
    }
      
    public function connexionPageAction(Request $req){
       //dump ("hello");
        //die();
         $objetAuthentification = $this->get('security.authentication_utils'); 
         $errors = $objetAuthentification->getLastAuthenticationError();
         $login = $objetAuthentification->getLastUsername();
         
         $vars = ['errors' => $errors,
            'login' => $login];
        
        return $this->render ('NurseryBundleViews/UserControllerViews/connexionPage.html.twig',$vars);   
    }
    

    
    public function seDeconnecterAction (){
        return $this->render ('NurseryBundleViews/UserControllerViews/se_deconnecter.html.twig');

    }
    
      public function afficherUtilisateursAction ()
    {
        $em=$this->getDoctrine()->getManager();
        
        $utilisateurs=$em->getRepository('NurseryBundle:Utilisateur')->findAll();
        
         return $this->render("NurseryBundleViews/UserControllerViews/afficher_utilisateurs.html.twig", array('user'=>$utilisateurs));
    }
    
    
    
    // envoyez une notification apres examen de la demande
    
    public function messageCreateAction(Request $req)
    {
        $message = new Message();
        $formulaire= $this->createForm(MessageType::class,  $message );
        $formulaire->handleRequest($req);
  
      if ($formulaire->isSubmitted() && $formulaire->isValid()){
       

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
             //redirect vers la page liste des puricultrice a cree
            return $this->redirectToRoute('afficherUtilisateurs');

       
        }
        else{
        
            return $this->render("NurseryBundleViews/UserControllerViews/ajout_message.html.twig",
                array ('formulaire'=>$formulaire->createView()));
               
        }
    }
    
   
}
