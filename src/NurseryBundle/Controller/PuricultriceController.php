<?php

namespace NurseryBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NurseryBundle\Entity\Puricultrice;
use NurseryBundle\Form\PuricultriceType;
use NurseryBundle\Entity\Groupe;
 
class PuricultriceController extends Controller
{
    public function ajoutPuricultriceAction(Request $req)
    {
        $puricultrice=new Puricultrice();
        
        $formulaire= $this->createForm(PuricultriceType::class, $puricultrice, array(
           'method'=>'POST',
           'action'=>$this->generateUrl('ajoutPuricultrice')
                ));
        $formulaire->handleRequest($req);
        
        if ($formulaire->isSubmitted() && $formulaire->isValid()){
       

            $em = $this->getDoctrine()->getManager();
            $em->persist($puricultrice);
            $em->flush();
             //redirect vers la page liste des puricultrice a cree
            return $this->redirectToRoute('afficherPuricultrices');

       
        }
        else{
        
            return $this->render("NurseryBundleViews/PuricultriceControllerViews/ajout_puricultrice.html.twig",
                array ('formulaire'=>$formulaire->createView()));
               
        }
        
    }  
    
       public function modifierPuricultriceAction (Request $req)
    {
       
       
        $em =$this->getDoctrine()->getManager();
        $puricultrice= $em->getRepository('NurseryBundle:Puricultrice')->findOneById($req->get('id'));
        
        
           
        if (is_null($puricultrice)){
//            dump($puricultrice);
//           die();
        
            return $this->redirectToRoute('ajoutPuricultrice');
        }
        
        $form=$this->createForm("NurseryBundle\Form\PuricultriceType", $puricultrice, 
                                                array('method'=>'POST',
                                                      'action'=>'/modifierPuricultrice'
                                                    )
                                );
        $form->handleRequest($req);
        
        
        // si on a clique Submit
        if ( $form->isSubmitted() &&  $form->isValid()){
           $puricultriceModifie = $form->getData();         
           $em->persist($puricultriceModifie);     
           $em->flush();
           //$message="l'entité a été modifié avec success!!";
           return $this->redirectToRoute ("afficherPuricultrices");
                
        }
        // afficher le formulaire
        return $this->render("NurseryBundleViews/PuricultriceControllerViews/modifier_puricultrice.html.twig", array(
            'form'=>$form->createView(),
            //'msg'=>$message,
            'idPuric'=>$req->get('id')
        ));
    }
    
      public function supprimerPuericultriceAction(Request $req)        
    {
     

        $em=$this->getDoctrine()->getManager();
        $Pueric= $em->find('NurseryBundle:Puricultrice', $req->get('id'));
        if(!$Pueric)
        {
            throw $this->createNotFoundException('le Personnel avec l\'id' .$req->get('id').  'n\'existe pas');
        }
        $em->remove($Pueric);
        $em->flush();

         return $this->redirectToRoute ("afficherPuricultrices");


    }
   //liste des Personnelles par groupe
     public function listPuricultriceParGroupeAction(Request $request)
    {

        $id =$request->get('id');
        $puricultrices = $this->getDoctrine()->getRepository("NurseryBundle:Puricultrice")->findBy(array('groupe' => $id));
        return $this->render('NurseryBundleViews/PuricultriceControllerViews/list_puricultrice_ groupe.html.twig', array('puricultrices' => $puricultrices, 'id' => $id));
    }
    
    public function afficherPuricultricesAction ()
    {
        $em=$this->getDoctrine()->getManager();
        $puricultrices=$em->getRepository('NurseryBundle:Puricultrice')->findAll();  
         return $this->render("NurseryBundleViews/PuricultriceControllerViews/afficher_puricultrices.html.twig", array('ListePuricultrice'=>$puricultrices));
    }
        
        
        
        
   
    
}

