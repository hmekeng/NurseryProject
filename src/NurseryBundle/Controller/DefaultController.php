<?php

namespace NurseryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function accueilAction()
    {
        return $this->render('NurseryBundleViews/Default/index.html.twig');
    }
}
