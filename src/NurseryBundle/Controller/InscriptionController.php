<?php
namespace NurseryBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NurseryBundle\Entity\Inscription;
use NurseryBundle\Form\InscriptionType;
use NurseryBundle\Entity\Groupe;
use NurseryBundle\Form\InscriptionEditType;
 
class InscriptionController extends Controller
{
  
    //cette premiere action permet de cree notre formulaire
    public function formViewInscriptionAction()
    {
      
       $inscription=new Inscription();
           
    
//        $UserForm = $this->createForm(UtilisateurType::class, new Utilisateur());

        $formInscription = $this ->createForm("NurseryBundle\Form\InscriptionType", $inscription, array(
           'method'=>'POST',
           'action'=>$this->generateUrl('ajouterDemande')
//               // ajouterDemande ceci est mis dans routing et correspond à l'id'
        ));
        
        
        $vars=['monFormDemande' => $formInscription->createView()];
        
        return $this->render("NurseryBundleViews/InscriptionControllerViews/form_view_inscription.html.twig", $vars);
       
    }
    
    
    // cette partie fait le traitement du formulaire creer plus haut qui permet d'inserer les utilisateurs
       public function ajouterDemandeAction(Request $req){
         
         $formInscription  = $this->createForm("NurseryBundle\Form\InscriptionType");
        
        $formInscription ->handleRequest($req);
        //dump($req);
//         // faire appel à handle request pour traiter la requete

        if ( $formInscription->isSubmitted() &&  $formInscription->isValid()){
           $formInscription->getData(); 
            //dump pour affiche
//            dump ( $formInscription->getData());
//            
           $demande =$formInscription->getData();
//           dump ($demande);
//           die();
           $demande->setUtilisateur ($this->getUser());
            $em=$this->getDoctrine()->getManager();
            
            //ici faut plutôt chercher à affecter les enfants selon leur âge  calculer d abord son age et ensuite inserer
           $bebeAll=$em->getRepository(Inscription::class)->findBy(array('groupe' =>$demande->getGroupe()));
           if(count($bebeAll)<15){
               $groupe1=$em->getRepository(Groupe::class)->find(1);
////               $groupe2=$em->getRepository(Groupe::class)->find(2);
////               $groupe3=$em->getRepository(Groupe::class)->find(3);
                $groupe1AllBebe=$em->getRepository(Inscription::class)->findBy(array('groupe' => $groupe1));
                if(count($groupe1AllBebe)<5){
                    $demande->setGroupe($groupe1);
//               }
////               elseif (count($groupe1AllBebe)<=6 && count($groupe1AllBebe)<10) {
////               $demande->setGroupe($groupe2);}
//           
//          
           }

//           $groupe1=$em->getRepository(Groupe::class)->find(1);
          
           
            //dump ($demande->getUtilisateur());
            //die();
            // stocker dans la BD, par exemple
            $em = $this->getDoctrine()->getManager();
            $em->persist($demande);
            $em->flush();
        }
        //ici on va faire un redirect vers successpage
           return $this->redirectToRoute ("successPage");
    }
   }
    
    
    public function successPageAction()       
    {
        return $this->render("NurseryBundleViews/InscriptionControllerViews/success_page.html.twig");
    }
    
    public function afficherInscriptionsAction ()
    {
        $em=$this->getDoctrine()->getManager();
        $inscriptions=$em->getRepository('NurseryBundle:Inscription')->findAll();
        //$vars = ['uneInscription'=>$inscription];   
         return $this->render("NurseryBundleViews/InscriptionControllerViews/afficher_inscriptions.html.twig", array('uneInscription'=>$inscriptions));
    }
    
    public function modifierInscriptionAction (Request $req)
    {
       
       
        $em =$this->getDoctrine()->getManager();
        $inscription= $em->getRepository('NurseryBundle:Inscription')->findOneById($req->get('id'));
        
        
           
        if (is_null($inscription)){
            dump($inscription);
           die();
        
            return $this->redirectToRoute('connexionPage');
        }
        
        $form=$this->createForm("NurseryBundle\Form\InscriptionType", $inscription, 
                                                array('method'=>'POST',
                                                      'action'=>'/modifierInscription'
                                                    )
                                );
        $form->handleRequest($req);
        
        
        // si on a clique Submit
        if ( $form->isSubmitted() &&  $form->isValid()){
           $inscriptionModifie = $form->getData();         
           $em->persist($inscriptionModifie);     
           $em->flush();
           //$message="l'entité a été modifié avec success!!";
           return $this->redirectToRoute ("afficherInscriptions");
                
        }
        // afficher le formulaire
        return $this->render("NurseryBundleViews/InscriptionControllerViews/modifier_inscription.html.twig", array(
            'form'=>$form->createView(),
            //'msg'=>$message,
            'idInscription'=>$req->get('id')
        ));
    }
    public function supprimerInscriptionAction(Request $req)        
    {
        //$message="supprimmer une inscription";


        $em=$this->getDoctrine()->getManager();
        $inscription= $em->find('NurseryBundle:Inscription', $req->get('id'));
        if(!$inscription)
        {
            throw $this->createNotFoundException('l\'inscription avec l\'id' .$req->get('id').  'n\'existe pas');
        }
        $em->remove($inscription);
        $em->flush();

        //$message="la demande a été bien supprimée!!!";

        //return new Response ("la demande a été bien supprimée!!!");

         return $this->redirectToRoute ("afficherInscriptions");


    }
    
  
    public function  afficherUneInscriptionAction($id) 
    {

        $em=$this->getDoctrine()->getManager();

        $inscription=$em->getRepository('NurseryBundle:Inscription')->find($id);

        if (!$inscription) {
          throw $this->createNotFoundException(
          'pas de demande pour cette id '.$id
            );
         }

         // return new Response('la demande est: '.$inscription->getNomEnfant());

        return $this->render("NurseryBundleViews/InscriptionControllerViews/afficher_une_inscription.html.twig", array('uneInscription'=>$inscription) );
    }
    
    // liste inscription par utilisateur

    public function listInscripionParUsertshowAction(Request $request)
    {

        $id =$request->get('id');

        $inscriptions = $this->getDoctrine()->getRepository("NurseryBundle:Inscription")->findBy(array('utilisateur' => $id));
        return $this->render('NurseryBundleViews/InscriptionControllerViews/list_inscripion_ user_show.html.twig', array('inscriptions' => $inscriptions, 'id' => $id));
    }
    
    
     public function GestionInscriptionAction(Request $request)
    {
       

        return $this->render('NurseryBundleViews/InscriptionControllerViews/Gestion_inscription.html.twig');
    }
    
     public function inscriptionGroupeAction(Request $req)
    {
        $inscription=new Inscription();
           
        $formulaire= $this->createForm(InscriptionEditType::class, $inscription);
        $formulaire->handleRequest($req);
        
        if ($formulaire->isSubmitted() && $formulaire->isValid()){
       

            $em = $this->getDoctrine()->getManager();
            $em->persist($inscription);
            $em->flush();
             //redirect vers la page liste des groupes a cree
             return $this->redirectToRoute ("afficherInscriptions");

       
        }
        else{
        
            return $this->render("NurseryBundleViews/InscriptionControllerViews/inscription_par_groupe.html.twig",
            array ('formulaire'=>$formulaire->createView()));
               
        }
    }
}
                          
     


