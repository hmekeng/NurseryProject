<?php

namespace NurseryBundle\Entity;

/**
 * Puricultrice
 */
class Puricultrice
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nomPuric;

    /**
     * @var string
     */
    private $prenomPuric;

    /**
     * @var string
     */
    private $adresse;

    /**
     * @var string
     */
    private $codePostal;

    /**
     * @var string
     */
    private $commune;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */
    private $sexe;

    /**
     * @var string
     */
    private $diplome;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomPuric
     *
     * @param string $nomPuric
     *
     * @return Puricultrice
     */
    public function setNomPuric($nomPuric)
    {
        $this->nomPuric = $nomPuric;

        return $this;
    }

    /**
     * Get nomPuric
     *
     * @return string
     */
    public function getNomPuric()
    {
        return $this->nomPuric;
    }

    /**
     * Set prenomPuric
     *
     * @param string $prenomPuric
     *
     * @return Puricultrice
     */
    public function setPrenomPuric($prenomPuric)
    {
        $this->prenomPuric = $prenomPuric;

        return $this;
    }

    /**
     * Get prenomPuric
     *
     * @return string
     */
    public function getPrenomPuric()
    {
        return $this->prenomPuric;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Puricultrice
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Puricultrice
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set commune
     *
     * @param string $commune
     *
     * @return Puricultrice
     */
    public function setCommune($commune)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Get commune
     *
     * @return string
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Puricultrice
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Puricultrice
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Puricultrice
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set diplome
     *
     * @param string $diplome
     *
     * @return Puricultrice
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return string
     */
    public function getDiplome()
    {
        return $this->diplome;
    }
    /**
     * @var \NurseryBundle\Entity\Groupe
     */
    private $groupe;


    /**
     * Set groupe
     *
     * @param \NurseryBundle\Entity\Groupe $groupe
     *
     * @return Puricultrice
     */
    public function setGroupe(\NurseryBundle\Entity\Groupe $groupe = null)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return \NurseryBundle\Entity\Groupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}
