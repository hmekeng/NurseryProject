<?php

namespace NurseryBundle\Entity;

/**
 * Inscription
 */
class Inscription
{
    /**
     * @var string
     */
    private $nomEnfant;

    /**
     * @var string
     */
    private $nomParent;

    /**
     * @var string
     */
    private $prenomParent;

    /**
     * @var string
     */
    private $adresse;

    /**
     * @var string
     */
    private $codePostal;

    /**
     * @var string
     */
    private $commune;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var \DateTime
     */
    private $dateNaiss;

    /**
     * @var \DateTime
     */
    private $dateEntree;

    /**
     * @var string
     */
    private $presence;

    /**
     * @var string
     */
    private $section;

    /**
     * @var \DateTime
     */
    private $dateDemande;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $documents;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;

    /**
     * @var \NurseryBundle\Entity\Utilisateur
     */
    private $utilisateur;

    /**
     * @var \NurseryBundle\Entity\Groupe
     */
    private $groupe;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nomEnfant
     *
     * @param string $nomEnfant
     *
     * @return Inscription
     */
    public function setNomEnfant($nomEnfant)
    {
        $this->nomEnfant = $nomEnfant;

        return $this;
    }

    /**
     * Get nomEnfant
     *
     * @return string
     */
    public function getNomEnfant()
    {
        return $this->nomEnfant;
    }

    /**
     * Set nomParent
     *
     * @param string $nomParent
     *
     * @return Inscription
     */
    public function setNomParent($nomParent)
    {
        $this->nomParent = $nomParent;

        return $this;
    }

    /**
     * Get nomParent
     *
     * @return string
     */
    public function getNomParent()
    {
        return $this->nomParent;
    }

    /**
     * Set prenomParent
     *
     * @param string $prenomParent
     *
     * @return Inscription
     */
    public function setPrenomParent($prenomParent)
    {
        $this->prenomParent = $prenomParent;

        return $this;
    }

    /**
     * Get prenomParent
     *
     * @return string
     */
    public function getPrenomParent()
    {
        return $this->prenomParent;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Inscription
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Inscription
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set commune
     *
     * @param string $commune
     *
     * @return Inscription
     */
    public function setCommune($commune)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Get commune
     *
     * @return string
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Inscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Inscription
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set dateNaiss
     *
     * @param \DateTime $dateNaiss
     *
     * @return Inscription
     */
    public function setDateNaiss($dateNaiss)
    {
        $this->dateNaiss = $dateNaiss;

        return $this;
    }

    /**
     * Get dateNaiss
     *
     * @return \DateTime
     */
    public function getDateNaiss()
    {
        return $this->dateNaiss;
    }

    /**
     * Set dateEntree
     *
     * @param \DateTime $dateEntree
     *
     * @return Inscription
     */
    public function setDateEntree($dateEntree)
    {
        $this->dateEntree = $dateEntree;

        return $this;
    }

    /**
     * Get dateEntree
     *
     * @return \DateTime
     */
    public function getDateEntree()
    {
        return $this->dateEntree;
    }

    /**
     * Set presence
     *
     * @param string $presence
     *
     * @return Inscription
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;

        return $this;
    }

    /**
     * Get presence
     *
     * @return string
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Set section
     *
     * @param string $section
     *
     * @return Inscription
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set dateDemande
     *
     * @param \DateTime $dateDemande
     *
     * @return Inscription
     */
    public function setDateDemande($dateDemande)
    {
        $this->dateDemande = $dateDemande;

        return $this;
    }

    /**
     * Get dateDemande
     *
     * @return \DateTime
     */
    public function getDateDemande()
    {
        return $this->dateDemande;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add document
     *
     * @param \NurseryBundle\Entity\Message $document
     *
     * @return Inscription
     */
    public function addDocument(\NurseryBundle\Entity\Message $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NurseryBundle\Entity\Message $document
     */
    public function removeDocument(\NurseryBundle\Entity\Message $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add message
     *
     * @param \NurseryBundle\Entity\Message $message
     *
     * @return Inscription
     */
    public function addMessage(\NurseryBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \NurseryBundle\Entity\Message $message
     */
    public function removeMessage(\NurseryBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set utilisateur
     *
     * @param \NurseryBundle\Entity\Utilisateur $utilisateur
     *
     * @return Inscription
     */
    public function setUtilisateur(\NurseryBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \NurseryBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set groupe
     *
     * @param \NurseryBundle\Entity\Groupe $groupe
     *
     * @return Inscription
     */
    public function setGroupe(\NurseryBundle\Entity\Groupe $groupe = null)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return \NurseryBundle\Entity\Groupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}

