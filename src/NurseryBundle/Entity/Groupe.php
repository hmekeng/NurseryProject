<?php

namespace NurseryBundle\Entity;

/**
 * Groupe
 */
class Groupe
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nomGroupe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomGroupe
     *
     * @param string $nomGroupe
     *
     * @return Groupe
     */
    public function setNomGroupe($nomGroupe)
    {
        $this->nomGroupe = $nomGroupe;

        return $this;
    }

    /**
     * Get nomGroupe
     *
     * @return string
     */
    public function getNomGroupe()
    {
        return $this->nomGroupe;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscriptions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inscriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add inscription
     *
     * @param \NurseryBundle\Entity\Inscription $inscription
     *
     * @return Groupe
     */
    public function addInscription(\NurseryBundle\Entity\Inscription $inscription)
    {
        $this->inscriptions[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \NurseryBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\NurseryBundle\Entity\Inscription $inscription)
    {
        $this->inscriptions->removeElement($inscription);
    }

    /**
     * Get inscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $puricultrices;


    /**
     * Add puricultrice
     *
     * @param \NurseryBundle\Entity\Puricultrice $puricultrice
     *
     * @return Groupe
     */
    public function addPuricultrice(\NurseryBundle\Entity\Puricultrice $puricultrice)
    {
        $this->puricultrices[] = $puricultrice;

        return $this;
    }

    /**
     * Remove puricultrice
     *
     * @param \NurseryBundle\Entity\Puricultrice $puricultrice
     */
    public function removePuricultrice(\NurseryBundle\Entity\Puricultrice $puricultrice)
    {
        $this->puricultrices->removeElement($puricultrice);
    }

    /**
     * Get puricultrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPuricultrices()
    {
        return $this->puricultrices;
    }
}
